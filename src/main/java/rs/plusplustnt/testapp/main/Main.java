/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.plusplustnt.testapp.main;

import rs.plusplustnt.testapp.communication.Communication;
import rs.plusplustnt.testapp.controller.Controller;

/**
 *
 * @author Anja
 */
public class Main {

    public static void main(String[] args) {

        try {
            Communication.getInstance();
            Controller.getInstance().readPackagesFromJSONFile();
            Controller.getInstance().removeExpiredPackages();
            Controller.getInstance().startClient();

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        Controller.getInstance().writePackagesInJSONFIle();
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("An error occurred while writing in JSON file."+e.getMessage()); 
                    }

                    System.out.println(" --------> Goodbuy dear client! Your packages have been saved in the JSON file and restored when you come again! <--------");
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("An error occurred! "+ex.getMessage());
        }
    }
}
