/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.plusplustnt.testapp.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Anja
 */
public class Package implements Serializable{
    private long idPackage;
    private long length;
    private long id;
    private long delay;
    private Date creationTime;
    private Date expirationTime;

    public Package() {
    }
    

    public Package(long idPackage, long length, long id, long delay, Date creationTime, Date expirationTime) {
        this.idPackage = idPackage;
        this.length = length;
        this.id = id;
        this.delay = delay;
        this.creationTime = creationTime;
        this.expirationTime = expirationTime;
    }
    
    public Package(byte[] finalyB) {
        this.idPackage = finalyB[0];
        this.length = finalyB[4];
        this.id = finalyB[8];
        this.delay = finalyB[12];

        Calendar calendar = Calendar.getInstance();
        this.setCreationTime(calendar.getTime());
        calendar.add(Calendar.SECOND, (int) this.getDelay());
        this.setExpirationTime(calendar.getTime());
    }

    public long getIdPackage() {
        return idPackage;
    }

    public void setIdPackage(long idPackage) {
        this.idPackage = idPackage;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    @Override
    public String toString() {
        return "Package{" + "idPackage=" + idPackage + ", length=" + length + ", id=" + id + ", delay=" + delay + ", creationTime=" + creationTime + ", expirationTime=" + expirationTime + '}';
    }
    
    
    
}
