/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.plusplustnt.testapp.thread;

import rs.plusplustnt.testapp.communication.Communication;
import rs.plusplustnt.testapp.controller.Controller;

/**
 *
 * @author Anja
 */
public class ClientThread extends Thread {

    private boolean run;

    public ClientThread() {
        run = true;
    }

    @Override
    public void run() {
        try {
            System.out.println("----------------> RECEIVING PACKAGES FROM SERVER IS IN PROCESS <----------------");
            System.out.println("---------------------> It would last for next 10 seconds <---------------------");
            while (run) {
                Communication.getInstance().receive();
            }
            Controller.getInstance().exitApplication();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("An error occurred while receiveing packages from server."+e.getMessage());
        }
    }

    public void setRun(boolean b) {
        this.run = b;
    }
}
