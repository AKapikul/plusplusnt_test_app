/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.plusplustnt.testapp.thread;

import rs.plusplustnt.testapp.communication.Communication;
import rs.plusplustnt.testapp.controller.Controller;
import rs.plusplustnt.testapp.domain.Package;

/**
 *
 * @author Anja
 */
public class PackageThread extends Thread{
    
    
    private Package pack;
    
    public PackageThread(Package pack) {
        this.pack = pack;
    }

    @Override
    public void run() {
        try {
            Communication comm = Communication.getInstance();

            synchronized (this) {
                this.wait(this.pack.getDelay() * 1000);
            }

            Controller.getInstance().getPackages().remove(this.pack);
            comm.send(this.pack);

        } 
        catch (InterruptedException ex) {
             ex.printStackTrace();
            System.out.println("An error occurred while while thread has been waiting!"+ex.getMessage());
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("An error occurred in communication!"+e.getMessage());
        }
    }
}
