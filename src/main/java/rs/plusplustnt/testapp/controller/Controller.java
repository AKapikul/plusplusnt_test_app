/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.plusplustnt.testapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import rs.plusplustnt.testapp.communication.Communication;
import rs.plusplustnt.testapp.json.JsonFileOperation;
import rs.plusplustnt.testapp.thread.ClientThread;
import rs.plusplustnt.testapp.domain.Package;

/**
 *
 * @author Anja
 */
public class Controller {

    private static Controller instance;
    private final Communication communication;
    private final JsonFileOperation operation;
    private final List<Package> packages;

    public Controller() throws Exception {
        communication = Communication.getInstance();
        operation = new JsonFileOperation();
        packages = new ArrayList<>();
    }

    public static Controller getInstance() throws Exception {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void startClient() {
        ClientThread client = new ClientThread();
        client.start();
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            int i = 10; // Time in seconds

            public void run() {
                i--;
                if (i < 0) {
                    timer.cancel();
                    client.setRun(false);
                }
            }
        }, 0, 1000);
    }

    public void readPackagesFromJSONFile() throws Exception {
        operation.JsonFileReader();
    }

    public void writePackagesInJSONFIle() throws Exception {
        operation.JsonFileWriter();
    }

    public void removeExpiredPackages() throws Exception {
        String message;
        System.out.println("==============================================");
        System.out.println("ATENTION: Expired packages would be deleted: ");
        for (int i = 0; i < packages.size(); i++) {
            if (packages.get(i).getExpirationTime().before(new Date())) {
                message = "Package with ID: " + packages.get(i).getId() + " is expired!";
                System.out.println(message);
                Communication.getInstance().send(message);
                packages.remove(i--);
            }
        }

        System.out.println("==============================================");
        if (packages.isEmpty()) {
            System.out.println("The package list is empty now!");
        } else {
            System.out.println("Packages that remained in the list:");
            for (Package package1 : packages) {
                System.out.println("Package: " + package1);
            }
        }
        System.out.println("==============================================");
    }

    public void exitApplication() {
        System.out.println("----------------> The program will shut down, received packages will be saved in a file! <----------------");
        System.exit(0);
    }
}
