/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.plusplustnt.testapp.json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import rs.plusplustnt.testapp.controller.Controller;
import rs.plusplustnt.testapp.domain.Package;

/**
 *
 * @author Anja
 */
public class JsonFileOperation {

    private static List<Package> list = new ArrayList<>();
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

    public void JsonFileReader() throws Exception {
        list = Controller.getInstance().getPackages();

        JSONParser parser = new JSONParser();
        try (FileReader reader = new FileReader("myJson.json")) {
            Object obj = parser.parse(reader);
            JSONArray packList = (JSONArray) obj;

            packList.forEach(packk -> parsePackObj((JSONObject) packk));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new Exception("An error occurred while reading from JSON file." + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("An error occurred in JSON file reader. " + e.getMessage());
        }
    }

    private static void parsePackObj(JSONObject pack) {
        try {
            JSONObject jsonObject = (JSONObject) pack.get("pack");
            long delay = (long) jsonObject.get("delay");
            Date creationTime = sdf.parse(jsonObject.get("creationTime").toString());
            Date expirationTime = sdf.parse(jsonObject.get("expirationTime").toString());
            long length = (long) jsonObject.get("length");
            long id = (long) jsonObject.get("id");
            long idPackage = (long) jsonObject.get("idPackage");

            Package p = new Package(idPackage, length, id, delay, creationTime, expirationTime);
            list.add(p);

        } catch (ParseException ex) {
            ex.printStackTrace();
            System.out.println("An error occurred while parsing the JSON object. " + ex.getMessage());
        }
    }

    public void JsonFileWriter() throws Exception {
        list = Controller.getInstance().getPackages();
        JSONArray packList = new JSONArray();
        for (Package aPackage : list) {

            JSONObject obj = new JSONObject();
            obj.put("idPackage", aPackage.getIdPackage());
            obj.put("length", aPackage.getLength());
            obj.put("id", aPackage.getId());
            obj.put("delay", aPackage.getDelay());
            obj.put("creationTime", sdf.format(aPackage.getCreationTime()));
            obj.put("expirationTime", sdf.format(aPackage.getExpirationTime()));

            JSONObject packs = new JSONObject();
            packs.put("pack", obj);

            packList.add(packs);
        }

        try (FileWriter file = new FileWriter("myJson.json")) {
            file.write(packList.toJSONString());
            file.flush();
        } catch (IOException ioex) {
            ioex.printStackTrace();
            throw new Exception("An error occurred while writing in JSON file." + ioex.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("An error occurred in JSON file writer. " + e.getMessage());
        }
    }

}
