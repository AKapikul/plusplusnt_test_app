/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.plusplustnt.testapp.communication;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import rs.plusplustnt.testapp.controller.Controller;
import rs.plusplustnt.testapp.domain.Package;
import rs.plusplustnt.testapp.thread.PackageThread;

/**
 *
 * @author Anja
 */
public class Communication {

    private final Socket soket;
    private static Communication instance;

    public Communication() throws IOException {
        soket = new Socket("hermes.plusplus.rs", 4000);
    }

    public static Communication getInstance() throws Exception {
        if (instance == null) {
            instance = new Communication();
        }
        return instance;
    }

    public Socket getSoket() {
        return soket;
    }

    public synchronized Object receive() throws Exception {
        try {
            
            DataInputStream in = new DataInputStream(soket.getInputStream());

            byte[] idPack = new byte[]{in.readByte()};
            System.out.println("The package ID is: " + idPack[0]);
            byte[] finalyB = new byte[]{};

            int capacity = 0;
            switch (idPack[0]) {
                case 1:
                    System.out.println("The received package is DUMMY PACKAGE:");
                    capacity = 16;
                    byte[] byteovi = new byte[capacity - 1];
                    in.read(byteovi, 0, capacity - 1);
                    finalyB = new byte[idPack.length + byteovi.length];
                    System.arraycopy(idPack, 0, finalyB, 0, idPack.length);
                    System.arraycopy(byteovi, 0, finalyB, idPack.length, byteovi.length);
                    break;
                case 2:
                    System.out.println("The received package is CANCEL PACKAGE:");
                    Controller.getInstance().exitApplication();

                    break;
                default:
                    throw new Exception("Type of the package is unknown!");
            }

            for (byte b : finalyB) {
                System.out.println("Byte value: " + b);
            }
            Package pack = new Package(finalyB);

            List<Package> packages = Controller.getInstance().getPackages();
            packages.add(pack);

            PackageThread pt = new PackageThread(pack);
            pt.start();

            System.out.println("# # # # # # # # # # # # # # # # #   CURRENT STATE OF THE PACKAGE LIST   # # # # # # # # # # # # # # # # #");
            for (Package aPackage : packages) {
                System.out.println("PACKAGE: " + aPackage);
            }
            System.out.println("==========================================================================");

            return pack;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("An error occurred while receiving an object! " + ex.getMessage());
        }
    }

    public synchronized void send(Object objekat) throws Exception {
        try {
            ObjectOutputStream out = new ObjectOutputStream(soket.getOutputStream());
            out.writeObject(objekat);
            out.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("An error occurred while sending an object! " + ex.getMessage());
        }
    }
}
